This repository contains a directory skeleton for submitting the exercises in
Project and Training 1 (BTI3001/BTI3002).

Refer to training_CSBasics.pdf for further information.
