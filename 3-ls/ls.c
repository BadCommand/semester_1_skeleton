#include <stdio.h>
#include <dirent.h> // format of directory entries
#include <string.h>
#include <stdlib.h>

#ifdef __unix__
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#else
#include <sys\stat.h>
#endif

int main (int argc, char* argv[])
{
  // Initialize vars
  DIR *dr;
  struct dirent *de; // Pointer for directory entry
  struct stat s;
  char pathnameArr[2][50];

  /*
    ADD ON
    struct dirent **namelist; // sorting files
    int  n;
    n=scandir(".",&namelist,NULL,alphasort); // example of sorting files
   */

  if(argc < 1)
    {
      exit(EXIT_FAILURE);
    } 
  else if(argc == 1)
    {
      // opendir() returns a pointer of DIR type
      if((dr = opendir(".")) == NULL)  // opendir returns NULL if couldn't open directory
      {
	printf("Could not open current directory");
	return 0;
      }
      strcpy(pathnameArr[1], "");
    }
  else
    {
      // opendir() returns a pointer of DIR type.
      if((dr = opendir(argv[1])) == NULL) 
	{
	  printf("Could not open current directory");
	  return 0;
	}
      strcpy(pathnameArr[1], argv[1]);
      strcpy(pathnameArr[1], strcat(pathnameArr[1], "/"));
    }
  
  while ((de = readdir(dr)) != NULL)
    {
      //printf("%s\n",(*de).d_name);
      if ((*de).d_name[0] != 46) // Ignore all files with "." (ASCII 46)
	{
	  strcpy(pathnameArr[2], pathnameArr[1]);
	  strcpy(pathnameArr[2], strcat(pathnameArr[2], (*de).d_name));
	  printf("%s", (*de).d_name); // Print file ...
	  if(lstat((pathnameArr[2]), &s) != 0)
	    {
	      perror("lstat");
	      exit(EXIT_FAILURE);
	    }
	  else
	    {
	      // ... Print correct file type identifier
	      switch (s.st_mode & S_IFMT) // Type of file
		{
		case S_IFDIR: printf("/"); // Directory
		  break;
		case S_IFLNK: printf("@"); // Symbolic Link
		  break;
		default:
		  if (s.st_mode & S_IXUSR) // execute/search permission, owner
		  {
		    printf("*");
		  }
		}
	    }
	  printf("\n"); // next line
	}
    }
  closedir(dr);
  
  return 0;
}

