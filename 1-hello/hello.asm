SECTION .data			; Section containing initialised data
	initString: db "Hello world!",10
	initLen: equ $-initString
SECTION .bss			; Section containing uninitialized data	

SECTION .text			; Section containing code

global 	_start			; Linker needs this to find the entry point!
	
_start:
	nop			; This no-op keeps gdb happy...
	mov rax, 1		; code fo sys_write call
	mov rdi, 1		; specify file descriptor: standard output
	mov rsi, initString	; adress of the string to be written; pass offset of msg
	mov rdx, initLen	; length of the string; pass length of the msg
	syscall			; make kernel call
	
	mov rax, 60		; Code for exit
	mov rdi, 0 		; Return a code of zero
	syscall			; Make kernel call
