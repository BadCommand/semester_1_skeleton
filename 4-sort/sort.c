#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_SIZE 5

int outputFunction();

// Initialize main  vars
static bool sortRev = false;

int main (int argc, char **argv)
{
  // Initialize local vars
  static char* inputArr[MAX_SIZE];
  static int input = 0;

  if (argc == 2 && (strcmp("-r", argv[1]) == 0))
    {
      sortRev = true;
    }
  else if (argc > 2 || argc < 1)
    {
      printf("Invalid input arguments: Too many?\n");
      exit(EXIT_FAILURE);
    }
  else if (argc == 2 && (strcmp("-r", argv[1])) != 0)
    {
      printf("Invalid optional command\n");
      exit(EXIT_FAILURE);
    }
  
  while (!feof(stdin))
    {
      inputArr[input] = (char *)malloc(MAX_SIZE*2);
      scanf("%s\n", inputArr[input]);
      input++;
    }

  qsort(inputArr, input, sizeof(char *), outputFunction);
  
  for(int i = 0; i < input; i++)
    {
      printf("%s\n", inputArr[i]);
    }
  
  exit(EXIT_SUCCESS);
}

int outputFunction(const void *st, const void *sec)
{
  const char **pSt = (const char **)st;
  const char **pSec = (const char **)sec;

  if (sortRev == false)
    {
      return strcmp(*pSt, *pSec);
    }
  return strcmp(*pSec, *pSt);
}
