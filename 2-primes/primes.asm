	;; Author:	Florian M. Reinbold
	;; Date:	23.11.2019
	;; Description:	Get input value, check count all primes lower than input value and write counter as output in console.
	;; Version:	3.2
	;; Inspired and helped by Joel Urech

SECTION .data			; Section containing initialized data
	OutputMemory: dw "         UNUSED" ;; Create some space; only 9 bit in total needed
	OUTPUTVARLEN equ $-OutputMemory	      ; Length of OutputMemory
SECTION .bss			; Section containing uninitialized data

SECTION .text			; Section containing code	
	global _start		; Linker need this to find the entry point: IMPORTANT!

_start:
	nop			; Still keeps gdb happy
	xor ecx, ecx		; Clear registry ecx to 0 (32 bit enough): Counter of input data length
	mov rbx, [rsp+OUTPUTVARLEN]	; Copy address of first input argument to registry rbx

ConvertInput:			; Convert input values as long as current one is not a 0 integer (= stop reading!)
	mov al, [rbx+rcx]	; Move the first byte of addresses content sum from rbx and rcx on registry al (8 bit register enough)
	cmp al, 0		; Check if current index value is an integer of 0 (= Terminator!)
	jz PrepareSieving	; If al registry is 0 (ZF was set) continue at label
	inc ecx			; Increment length counter at registry ecx by 1
	add al, -48		; Converting from ASCII data type into integer data type
	push rax		; Put input data on the stack
	jmp ConvertInput	; Jump to label again and repeat converting
	
PrepareSieving:			; Set and initialize all needed registers
	xor ax, ax		; Clear registry ax to 0: Counter of input value data (16 bit enough)
	mov r8w, cx		; Copy current length counter value to registry r8w (16 bit enough)

StartSieving:			; Check if current integer is prime
	lea rdx, [rax*8+rax]    ; Multiply rax by 10 and put it on registry rdx
	add rax, rdx   		; Add both values together
	dec r8b			; Decrement length counter by 1 (8 bit enough)
	mov rbx, [rsp+r8*8]	; Pick next digit from the stack by scaling rsp by factor 8
	add al, bl		; Add new digit to previous digit in registry al (8 bit enough)
	dec cl			; Count backwards from cl: Value length counter (8 bit enough)
	jnz StartSieving	; Jump to label again and repeat picking and sieving
	mov r8, rax		; Keep value in "mind"
	cmp r8w, 3		; Compare value against 3 (16 bit enough)
	jnb Continue		; Jump to label and continue procedure
	mov r8b, 0		; No more primes under the number 3 (8 bit enough)
	jmp CountPrimes		; Jump to label for output preparing

	;;  Until here five registers just to be used

Continue:
    xor rax, rax
	xor cx, cx 		; Clear registry cx to 0 (16 bits enough)
	mov r9d, r8d 		; Copy value from "mind"
	shr r9, 7		; Divide by 128
	inc r9  		; Increment by 1 because of number of bits

PushZeros:			; Push 0 until number of needed bits reached
	push rax    		; Push 0 on the stack
	inc ecx			; Increment ecx by 1:
	cmp ecx, r9d		; Compare if needed number of bits reached
	jne PushZeros		; Jump to label and repeat 0 pushing
	xor rbp, rbp		; Clear registry di to 0: Byte counter
	xor r10, r10		; Clear registry dp to 0: Bit Position counter
	mov r11b, 3		; First to be tested number 3 at position (8 bit enough) byte 0/bit 0

ProcessBits:
	mov ax, [rsp+rbp]	; Pick 2 bytes from the stack
	bt ax, r10w		; Test for bits
	jnc TestByMultiplying	; Jump to label and start testing all primes

NextNumber:
	add r11, 2		; Add 2 to start with next number
	cmp r10w, 7		; Test if current position is last bit of current byte
	je IncrementBitCounter	; Jumpt to label for incrementing bit counter
	inc r10b		; Increment bit position counter by 1 (8 bit enough)
	jmp ProcessBits		; Jump to label for picking next byte from stack
	
IncrementBitCounter:
	inc bp			; Increment bit counter by 1 (16 bit still enough)
	xor r10, r10		; Clear bit position counter
	jmp ProcessBits		; Jump to label and repeat bit picking from next byte on the stack
TestByMultiplying:
	mov eax, r11d		; Initialize registry eax for some math (32 bit enough)
	mov ebx, r11d		; Initialize registry bx for some math (32 bit enough)
	mul ebx			; Do some multiplication
	cmp eax, r8d		; Test if square is lower than the current test number (32 bit enough)
	jnb Count		; Jump to label and start counting primes
	shr eax, 1		; Divide by 2
	mov r15d, eax		; Copy number of bit to registry eax

FindPrimes:
	mov r13, rsp		; Set r13 to stack pointer address: Offset 0
	mov ecx, r15d		; Copy of number of current bit number (32 bit enough)
	mov edx, r15d		; Copy of number of current bit number
	shr rdx, 3		; Divide by 8 for byte offset
	add r13, rdx		; Keep in "mind" offset
	lea rdx, [rdx*8]	; Multiply by 8
	sub rcx, rdx		; Substract with previous value: Bit position
	cmp ecx, 0		; Compare if it is 0
	jne CheckForOne		; Jump to label and check if it's value is 1
	mov rdx, 128		; Set bitmask because we have 0
	dec r13			; Decrement offset by 1
	jmp BitMasking		; Jumpt to label for preparing bitmask

CheckForOne:
	mov dl, 1		; Move 1 in registry dl (8 bit enough)
	cmp cl, 1		; Compare if bit position is 1 (8 bit enough)
	je BitMasking		; Jumpt to label for preparing bitmask
	dec cl			; Decrement by 1 because of including 1 in bl
	shl dl, cl		; Multiply by 1
	
BitMasking:			; Set an bit mask for later prime counting
	mov ax, [r13]		; Pick 2 bytes from stack
	or al, dl		; Change bits at bitmask position to 1
	mov [r13], ax		; Put data on stack again
	add r15, r11		; Set next bit to 1
	mov rax, r15		; Copy current bit number
	shl rax, 1		; Multiply bit number by 2
	inc rax			; Increment by 1: Current number of bits
	cmp rax, r8		; Compare with number of input
	jb FindPrimes		; Jumpt to label and check next odd multiple that is stored in registry
	jmp NextNumber		; Jump to label for the next odd number

Count: 
	xor rbx, rbx 		; Clear registry to 0

DoPopCount:
	pop rax			; Pull 64 bits from stack
	popcnt rax, rax		; Count all the bits with value 1
	add rbx, rax		; Keep in "mind" rax
	dec r9d			; Decrement by 1: Bit counter (32 bit enough)
	jnz DoPopCount		; Jumpt to label to repeat bit counting
	mov eax, r8d		; Put input number on eax (32 bit enough)
	test r8d, 1		; Test if input is odd or even number
	jz CountEvenNumber	; Jump to label for even number counting
	inc al			; Increment by 1: Get correct output (8 bit enough)

CountEvenNumber:    		; all in all 32 bit enough
	shr eax, 1		; Divide input number by 2
	add ebx, eax		; Put even number to the other ones
	sub r8d, ebx		; Substract all: We now have a result
	
CountPrimes:   			; Count primes by sum all bits and convert in decimal
	mov eax, r8d		; Save current number
	mov cx, 10		; Value  for converting in decimal number (16 bit enough)
	xor rbx, rbx		; Clear registry to 0
	mov dx, cx		; Set final value
	push rdx		; Put it on the stack

SomeMoreMath: 			; Divide until 0
	xor rdx, rdx		; Clear registry to 0
	div ecx			; Divide
	push rdx		; Push it on the stack
	inc bl      		; Increment by 1 (8 bit enough)
	test al, al		; Check if it's 0
	jnz SomeMoreMath	; Jump to label and repeat until it's 0
	xor rcx, rcx

TakeDigit:
	pop rax			; Pull rax from stack
	cmp rax, 10		; Final value?
	jz PrepareOutput	; Jump to label and prepare output
	add rax, 48		; Convert integer to ASCII data type
	mov [OutputMemory+ecx], al ; Copy final value to output variable
	inc cl			  ; Increment by 1  (8 bit enough)
	jmp TakeDigit		  ; Jump to label and repeat procedure
	
PrepareOutput:
	mov [OutputMemory+ecx], al ; Make copy of output
	cmp cx, OUTPUTVARLEN	   ; Compare if output var length is equal to output memory size
	je PrepareExit
	inc cx			  ; Increment by 1: Length of output variable (16 bit enough)
	
PrepareExit:
	mov rax, 1		; Code for sys_write call: Write output
	mov rdi, 1		; Code for standard output
	mov rsi, OutputMemory	; Adress of the string to be written
	mov dx, cx		; Length of the string
	syscall			; Make kernel call move equal to rax, 60
Exit:	
	mov rax, 60		; Code for exit
	mov rdi, 0		; Return a code of zero (means everything ok!)
	syscall			; Make kernel call

	nop			; Keeps gdb still happy
