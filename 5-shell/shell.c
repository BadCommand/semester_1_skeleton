#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

//size_t ist ein unsigned integer type
void* ealloc(void* prev, size_t size) {
	void* ptr = realloc(prev, size);
	assert(ptr != NULL);
  //assert(expression) expression hat den Wert Null wenn es ausgeführt wird
	return ptr;
}

char* b_trim(char* command) {
	if (NULL == command) return NULL;
	char *p = command;
    int l = strlen(command);
    while(isspace(p[l - 1])) p[--l] = 0;
    while(*p && (isspace(*p) || '\0' == *p)) ++p, --l;
    memmove(command, p, l + 1);
    return command;
}


char* b_line()
{
	static const char EXIT[] = "exit";

	char* command = NULL;
	unsigned long size = 0;
	write(STDOUT_FILENO, "$ ", 2);
	getline(&command, &size, stdin);
	if ('\0' == *command) return strdup(EXIT);
	command = b_trim(command);
	if ('\0' == *command) {
		free(command);
		return NULL;
	}
	return command;
}


char* br_token(char* command)
{
	static char first, *previous;

	if (command == NULL) command = previous;
	else first = command[0];
	if (!first) return NULL;

	char* token;
	command[0] = first;

	if ('$' != first) {
		token = command;
		while(command[0] && '$' != command[0]) command++;
	} else if('{' != command[1]) {
		token = command++;
		while(
			command[0] &&
			(isalnum(command[0]) || command[0] == '_' || command[0] == '?')
		) command++;
	} else {
		token = ++command;
		while(command[0] && '}' != command[0]) command++;
		if('}' == command[0]) *(command++) = '\0';
		*token = '$';
	}

	previous = command;
	first = command[0];
	command[0] = '\0';

	return token;
}

char* b_expand(char* command)
{
	size_t size = 0;
	char* buffer = NULL;
	while(command[0] && isspace(command[0])) command++;

	char* token = br_token(command);
	while(token != NULL) {
		if('$' == token[0]) token = getenv(token + 1);
		if(token != NULL) {
			size_t len = strlen(token);
			buffer = ealloc(buffer, (size + len + 1) * sizeof(char));
			strcpy(buffer + size, token);
			size += len;
		}
		token = br_token(NULL);
	}

	return buffer;
}

char* b_read()
{
	char *command = NULL, *expanded;
	do { command = b_line(); }
	while(NULL == command);
	expanded = b_expand(command);
	free(command);
	return expanded;
}


int b_isassign(char* command)
{
	if (!(isalnum(command[0]) || command[0] == '_' || command[0] == '?')) return 0;
	while(command[0] != '\0' && (isalnum(command[0]) || command[0] == '_' || command[0] == '?')) command++;
	return command[0] == '=';
}

void b_status(int status)
{
	char value[4];
	sprintf(value, "%d", status);
	setenv("?", value, 1);
}


void b_assign(char* command)
{
	char* key = strsep(&command, "=");
	b_status(setenv(key, command, 1));
}

char* b_token(char* command)
{
	static char *previous;

	if(NULL == command) command = previous;

	while(command && command[0] && isspace(command[0])) command++;
	if(NULL == command || '\0' == command[0]) return NULL;

	char* token;
	if ('"' == command[0]) {
		token = ++command;
		while(command[0] && '"' != command[0]) command++;
	} else {
		token = command;
		while(command[0] && !isspace(command[0])) command++;
	}

	if('\0' == command[0]) previous = NULL;
	else previous = command + 1;

	command[0] = '\0';
	return token;
}

//https://de.wikibooks.org/wiki/C-Programmierung:_Komplexe_Datentypen
typedef struct bshexec {
	pid_t id;
	struct bshexec* prev;
	int pipe[2];
	char* infile;
	char* outfile;
	char* errfile;
	int argc;
	char** argv;
} bshexec_t;

bshexec_t* bshexec_new(bshexec_t* previous)
{
	bshexec_t* proc = ealloc(NULL, sizeof(bshexec_t));

	proc->prev = NULL;
	proc->argc = 0;
	proc->argv = ealloc(NULL, sizeof(char*));
	proc->infile = proc->outfile = proc->errfile = NULL;

	if (NULL != previous) {
		proc->prev = previous;
		pipe(previous->pipe);
	} else {
		proc->pipe[0] = proc->pipe[1] = 0;
	}

	return proc;
}

void b_free(bshexec_t* proc)
{
	while (NULL != proc) {
		bshexec_t* prev = proc->prev;
		free(proc->argv);
		free(proc);
		proc = prev;
	}
}

void b_add(bshexec_t* proc, char* argument)
{
	proc->argv[proc->argc++] = argument;
	proc->argv = ealloc(proc->argv, (proc->argc + 1) * sizeof(char*));
	proc->argv[proc->argc] = NULL;
}

void b_fork(bshexec_t* proc)
{
	while(NULL != proc)
	{
		proc->id = fork();
		if (proc->id < 0) {
			perror("fork(): error\n");
			exit(EXIT_FAILURE);
		}

		if (0 != proc->id) {
			if (0 != proc->pipe[0]) {
				close(proc->pipe[0]);
				close(proc->pipe[1]);
			}
			proc = proc->prev;
			continue;
		}

    /*
       unistd.h
       STDIN_FILENO = 0
       STDOUT_FILENO = 1
       STDERR_FILENO = 2
    */


		if (NULL != proc->infile) {
			close(STDIN_FILENO);
			open(proc->infile, O_RDONLY);
		}


		if (NULL != proc->outfile) {
			close(STDOUT_FILENO);
			open(proc->outfile, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
		}


		if (NULL != proc->errfile) {
			close(STDERR_FILENO);
			open(proc->errfile, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
		}


		if (0 != proc->pipe[0]) {
			close(STDOUT_FILENO);
			close(proc->pipe[STDIN_FILENO]);
			dup2(proc->pipe[STDOUT_FILENO], STDOUT_FILENO);
			close(proc->pipe[STDOUT_FILENO]);
		}


		if (NULL != proc->prev) {
			close(STDIN_FILENO);
			close(proc->prev->pipe[STDOUT_FILENO]);
			dup2(proc->prev->pipe[STDIN_FILENO], STDIN_FILENO);
			close(proc->prev->pipe[STDIN_FILENO]);
		}

		execvpe(proc->argv[0], proc->argv, environ);
		perror("something went wrong\n");
		exit(EXIT_FAILURE);
	}
}

void b_wait(bshexec_t* proc)
{
	int status;
	while (NULL != proc) {
		waitpid(proc->id, &status, 0);
		proc = proc->prev;
	}
	b_status(WEXITSTATUS(status));
}

void bshexec(char* command)
{
	int inpipe = 0, inbackground = 0, infailure = 0;
	bshexec_t *proc = bshexec_new(NULL);

	char* token = b_token(command);
	while (NULL != token) {

		if (0 == strcmp(token, "&")) {
			inbackground = 1;
		} else if (0 == strcmp(token, "|")) {
			inpipe = 1;
			proc = bshexec_new(proc);
		} else if (0 == strcmp(token, "<")) {
			token = b_token(NULL);
			if (NULL == token) {
				infailure = 1;
				perror("something went wrong\n");
				break;
			}
			proc->infile = token;
		} else if (0 == strcmp(token, ">")) {
			token = b_token(NULL);
			if (NULL == token) {
				infailure = 1;
				perror("something went wrong\n");
				break;
			}
			proc->outfile = token;
		} else if (0 == strcmp(token, "2>")) {
			token = b_token(NULL);
			if (NULL == token) {
				infailure = 1;
				perror("something went wrong\n");
				break;
			}
			proc -> errfile = token;
		} else {
			b_add(proc, token);
		}

		token = b_token(NULL);
	}

	if (0 == proc->argc) {
		infailure = 1;
		perror("something went wrong\n");
	}

	if (inpipe && inbackground) {
		infailure = 1;
		perror("something went wrong\n");
	}

	if (!infailure) {
		b_fork(proc);
		if (!inbackground) b_wait(proc);
	}

	b_free(proc);
}



int main(int argc, char** argv)
{
	char* command = NULL;
	while(1)
	{
		command = b_read();

		if (0 == strcmp(command, "exit")) {
			free(command);
			break;
		}

		if (b_isassign(command)) {
			b_assign(command);
			free(command);
			continue;
		}

		bshexec(command);
		free(command);
	}
	return EXIT_SUCCESS;
}
